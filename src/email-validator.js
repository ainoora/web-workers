const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export const validate = (email) => {
  if (typeof email !== 'string') {
    return false;
  }
  const emailParts = email.split('@');
  if (emailParts.length !== 2) {
    return false;
  }
  if (emailParts[0].length < 1) {
    return false;
  }
  const emailEnding = emailParts[1];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}

export const validateAsync = (email) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(validate(email));
    }, 500);
  });
};

export const validateWithThrow = (email) => {
  if (validate(email)) {
    return true;
  } else {
    throw new Error('Invalid email address');
  }
};

export const validateWithLog = (email) => {
  const result = validate(email);
  console.log(`Validation result for ${email}: ${result}`);
  return result;
};
