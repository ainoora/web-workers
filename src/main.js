import './styles/style.css';
import { createJoinProgramSection } from './join-us-section.js';
import { createCommunitySection } from './community-section.js'

const SectionCreator = {
  create (type) {
    switch (type) {
      case 'standard':
        createJoinProgramSection('Join Our Program', 'Subscribe');
        break;
      case 'advanced':
        createJoinProgramSection(
          'Join Our Advanced Program',
          'Subscribe to Advanced Program'
        );
        break;
      default:
        throw new Error(`Invalid section type: ${type}`);
    }
  }
};

SectionCreator.create('standard');

createCommunitySection();
