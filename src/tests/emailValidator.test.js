/* eslint-disable no-unused-expressions */
/* eslint-disable no-magic-numbers */
import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import {
  validate,
  validateAsync,
  validateWithLog,
  validateWithThrow
} from '../email-validator.js';
import sinon from 'sinon';

describe('validate', () => {
  it('should return false if input is not a string', () => {
    expect(validate(null)).to.be.false;
    expect(validate(42)).to.be.false;
    expect(validate(undefined)).to.be.false;
    expect(validate({})).to.be.false;
    expect(validate([])).to.be.false;
  });

  it('should return false if input is an empty string', () => {
    expect(validate(' ')).to.be.false;
    expect(validate('')).to.be.false;
  });

  it('should return false if input before @ has less than 1 char', () => {
    expect(validate('@gmail.com')).to.be.false;
    expect(validate('@outlook.com')).to.be.false;
  });

  it('should return false if input has two @ adresses', () => {
    expect(validate('invalid@invalid.com@gmail.com')).to.be.false;
    expect(validate('invalid@invalid.com@outlook.com')).to.be.false;
  });

  it('should return false if input has invalid adresses', () => {
    expect(validate('invalid@gmail.kz')).to.be.false;
    expect(validate('invalid@outsee.com')).to.be.false;
  });

  it('should return true if input is valid', () => {
    expect(validate('valid@gmail.com')).to.be.true;
    expect(validate('valid@outlook.com')).to.be.true;
  });
});

describe('validateAsync', () => {
  it('should return true for valid email addresses', async () => {
    const result1 = await validateAsync('valid@gmail.com');
    const result2 = await validateAsync('valid@outlook.com');
    expect(result1).to.be.true;
    expect(result2).to.be.true;
  });

  it('should return false for invalid email addresses', async () => {
    const result1 = await validateAsync('bla');
    const result2 = await validateAsync('bla@bla');
    const result3 = await validateAsync('bla@bla.bla.');
    expect(result1).to.be.false;
    expect(result2).to.be.false;
    expect(result3).to.be.false;
  });
});

describe('validateWithThrow', () => {
  it('should return true for valid email addresses', () => {
    expect(validateWithThrow('valid@gmail.com')).to.be.true;
    expect(validateWithThrow('valid@outlook.com')).to.be.true;
  });

  it('should throw an error for invalid email addresses', () => {
    expect(() => validateWithThrow('bla')).to.throw('Invalid email address');
    expect(() => validateWithThrow('bla@bla')).to.throw('Invalid email address');
    expect(() => validateWithThrow('bla@bla.bla.')).to.throw('Invalid email address');
  });
});

describe('validateWithLog', () => {
  let consoleLogStub;

  before(() => {
    consoleLogStub = sinon.stub(console, 'log');
  });

  after(() => {
    consoleLogStub.restore();
  });

  it('should return true for valid email addresses and log the result', () => {
    const result1 = validateWithLog('valid@gmail.com');
    const result2 = validateWithLog('valid@outlook.com');
    expect(result1).to.be.true;
    expect(result2).to.be.true;
    expect(consoleLogStub.calledTwice).to.be.true;
    expect(consoleLogStub.getCall(0).args[0]).to.equal('Validation result for valid@gmail.com: true');
    expect(consoleLogStub.getCall(1).args[0]).to.equal('Validation result for valid@outlook.com: true');
  });

  it('should return false for invalid email addresses and log the result', () => {
    expect(validateWithLog('bla')).to.be.false;
    expect(validateWithLog('bla@bla')).to.be.false;
    expect(validateWithLog('bla@bla.bla.')).to.be.false;
    expect(consoleLogStub.callCount).to.equal(5);
    expect(consoleLogStub.getCall(2).args[0]).to.equal('Validation result for bla: false');
    expect(consoleLogStub.getCall(3).args[0]).to.equal('Validation result for bla@bla: false');
    expect(consoleLogStub.getCall(4).args[0]).to.equal('Validation result for bla@bla.bla.: false');
  });
});
