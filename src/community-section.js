/* eslint-disable no-undef */
/* eslint-disable max-len */
import { Card } from './card-component.js';

const createCommunitySection = () => {
  const communitySection = document.createElement('section');
  communitySection.classList.add('app-section');

  const h2Element = document.createElement('h2');
  h2Element.innerHTML = 'Big Community of <br/> People Like You ';
  h2Element.classList.add('app-title');

  const h3Element = document.createElement('h3');
  h3Element.innerHTML = 'We’re proud of our products, and we’re really excited <br /> when we get feedback from our users.';
  h3Element.classList.add('app-subtitle');

  const usersList = document.createElement('div');
  usersList.classList.add('app-section__users-list');

  communitySection.appendChild(h2Element);
  communitySection.appendChild(h3Element);
  communitySection.appendChild(usersList);

  async function getData () {
    try {
      const response = await fetch('http://localhost:3000/community');
      const result = await response.json();
      result.forEach((user) => {
        const card = Card(
          user.avatar,
          user.firstName,
          user.lastName,
          user.position
        );
        usersList.appendChild(card);
      });
    } catch (error) {
      alert(result.error);
    }
  }
  getData();

  document.addEventListener('DOMContentLoaded', function () {
    const footer = document.querySelector('.app-footer');
    footer.before(communitySection);
  });
}

export { createCommunitySection };
