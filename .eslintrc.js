module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: 'standard',
  overrides: [
    {
      files: ['*.js'],
      // rules: {
      //   semi: 'off',
      //   'no-console': ['error', { allow: ['warn', 'error'] }],
      //   camelcase: ['error', { allow: ['_snake_case'] }],
      //   'no-param-reassign': ['error', { props: false }],
      //   'no-use-before-define': ['error', { functions: false, variables: false }],
      //   'max-len': ['error', { code: 110 }],
      //   'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
      //   'no-underscore-dangle': ['error', { allow: ['__dirname'] }],
      //   'prefer-const': ['error', { destructuring: 'all', ignoreReadBeforeAssign: true }],
      //   'no-magic-numbers': ['error', { ignore: [0, 1, 2], enforceConst: true }]
      // }
    }
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  rules: {}
}
